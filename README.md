# Zone Java project - Docker deploiement
## Description
This repository contains a docker-compose.yml file and scripts to create needed databases.
Follow [this link](https://gitlab.com/zonejava/conception/-/blob/master/README.md) to see the project overview.
## Prerequisite
You need to install docker and docker-compose on your machine. You need an internet connection to use the app 
(need to reach a test keycloak authentication server. If you want to use your own then go to [overview](https://gitlab.com/zonejava/conception/-/blob/master/README.md)).
## Installation & Run
Go to root directory of docker-compose.yml file and run `docker-compose up -d`.

When all container will be launched, you can access to Zone Java in your browser at `localhost:4200`.
## Owner
Arnaud Laval - arnaudlaval33@gmail.com

The Zone Java project is developed as part of final project of DA Java cursus (OpenClassrooms)